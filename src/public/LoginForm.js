import React from 'react';
import './LoginForm.css';
//const axios = require('axios').default;

class LoginForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {login: '', password: '', auth: props.auth};
    
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleChangeLogin(event) {
         // console.log("Login changed:" +event.target.value);
        this.setState({login: event.target.value});
      }
      handleChangePassword(event) {
        //console.log("Password changed:" +event.target.value);
        this.setState({password: event.target.value});
      }
      async handleSubmit(event) {
        //TODO: POST login
        event.preventDefault();
        if(this.state.login==="superuser" && this.state.password==="masło"){
               //alert('Hello, ' + this.state.login);
              this.state.auth.authenticate(this.state.login,'admin');
        
          }else{
            /*
            axios.get('localhost:8080/login')
              .then(function (response) {
                // handle success
                return response;
              })
              .catch(function (error) {
                // handle error
                alert(error);
              })
              .then(function () {
                // always executed
              });
              */
            /*
            axios.post('localhost:8080/login', {
              username: this.state.login,
              password: this.state.password
            })
            .then(function (response) {
              alert(response);
            })
            .catch(function (error) {
              alert(error);
            });
            */
            this.state.auth.authenticateFail();
          }
        
      }

      render(){
        return(
            <div className="LoginForm"> 
              <h2 className="LoginForm-h2">LOGIN FORM</h2>
              <form className="LoginForm-form" onSubmit={this.handleSubmit}>
                    <label className="LoginForm-label" for="login">Username:</label><br/>
                    <input className="LoginForm-input" type="text" value={this.state.login} onChange={this.handleChangeLogin} id="login" name="login"/> <br/>
                    <label className="LoginForm-label" for="password">Password:</label><br/>
                    <input className="LoginForm-input" type="password" value={this.state.password} onChange={this.handleChangePassword} id="password" name="password"/><br/>
                    <button className="LoginForm-button" type="submit">LOGIN</button>
              </form>
            </div>
        );
      }
   
}

export default LoginForm;