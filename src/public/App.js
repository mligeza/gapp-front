import React from 'react';
import './App.css';

//public
import LoginForm from './LoginForm';

//users
import UsersView from '../user/UsersView';
import AddUserForm from '../user/AddUserForm';
import EditUserForm from '../user/EditUserForm';

//tasks
import TasksView from '../task/TasksView';
import AddTaskForm from '../task/AddTaskForm';
import DoTask from '../task/DoTask';

//classes
import AddClassForm from '../class/AddClassForm';
import ClassesView from '../class/ClassesView';

//assignments 
import AddAssignmentForm from '../assignment/AddAssignmentForm';
import AssignmentsView from '../assignment/AssignmentsView';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

//
//let history = useHistory();



//function 

export default class App extends React.Component {

  constructor(props) {
    super(props);
    //admin - MOCK OK
    
    //this.state={isAuthenticated: true, user: 'superuser', role: "ADMIN" };

    //teacher - MOCK OK
    this.state={isAuthenticated: true, user: 'frank', role: "TEACHER" };

    //student
    //this.state={isAuthenticated: true, user: 'knut', role: "STUDENT" };

    //USER - OK
    //this.state={isAuthenticated: false, user: 'someone', role: 'USER' };

    //not logged in - OK
    //this.state={isAuthenticated: false, user: '', role: '' };

    //TODO: validate data everywhere!
    //TODO: add requests
    //TODO: clean up CSS and upgrade look
    //DONE: do not show lists if not logged in
    
  }
  authenticate(user, role) {
    this.setState({isAuthenticated: true, user: user, role: role,

      selectedUserId:'', selectedUserEmail: "", selectedUserRole: "",
    })
    alert('Logged succesfully '+ user+ ' as '+role);
    
    //setTimeout(()=>{}, 100); // fake async
  }

  authenticateFail() {
    alert('Login failed!');
    this.setState({isAuthenticated: false});
     // fake async
  }

  signout(cb) {
    this.setState({isAuthenticated: false});
    
    //setTimeout(cb, 100);
  }
  
  handleEditUser(row){
    this.setState({
      selectedUserId: row.id, 
      selectedUserEmail: row.email,
      selectedUserRole: row.role,
    })
    //alert(this.state.selectedUserId +" "+this.state.selectedUserEmail+" "+this.state.selectedUserRole)
  }

  Home(){
    return <div>
      Welcome to eGeoApp!
    </div>
  }
  AuthError(){
   return <p className="error">You do not have permisson to do that!</p>
  }
  RegisterError(){
    return <p className="error">You are not able to register when you are logged in!</p>
   }
  render(){
  return (
    <div className="App" id="App">
       <Router>
          <header className="App-header">
              <h2 className="App-h2">GAPP: GeoApp</h2>
              {
              /**-----------------------------------------------------
               *  Navigation
               */
            }
              <nav>
              <ul>
                {this.state.isAuthenticated && this.state.role ==="ADMIN" &&
                  <li>
                    <Link to="/users">Users</Link>
                  </li>
                }
                {this.state.isAuthenticated && this.state.role ==="ADMIN" &&
                  <li>
                    <Link to="/classes">Classes</Link>
                  </li>
                }
                
                {this.state.isAuthenticated && 
                  <li>
                    <Link to="/tasks">Tasks</Link>
                  </li>
                }
                {this.state.isAuthenticated &&  
                  <li>
                    <Link to="/assignments">Assignments</Link>
                  </li>
                }
                {this.state.isAuthenticated ? 
                  <li>
                      
                    <button
                      onClick={() => {
                        this.signout();
                      }}>
                      Sign out
                    </button>
                  </li> : null}
              </ul>
            </nav>
            
          </header>

          
          <hr/>
            {// ----------------------------------------------------- -----------------------------------------------------
            }
            {//!this.state.isAuthenticated  ?  : null
            }
         
         <Switch>

           {
              /**-----------------------------------------------------
               *  Users
               */
            }
            <Route path="/" exact render={(data) => <div>{!this.state.isAuthenticated  ? <div><LoginForm auth={this}/><Link to="/adduser">REGISTER</Link></div> : <this.Home/>} </div>  }/>


            
          <Route path="/users" render={(data ) =><div> {(this.state.isAuthenticated && this.state.role ==="ADMIN") ?
            <div >
              <h2 className="UsersView-h2">USERS</h2>
              <UsersView app={this}/> 
            </div>  : <this.AuthError/>
          }</div>}/>
          
          <Route path="/adduser" render={(data ) => 
          !this.state.isAuthenticated ? <AddUserForm/>: <this.RegisterError/>
            } />
          <Route path="/edituser" render={(data ) =>
           this.state.isAuthenticated && this.state.role==="ADMIN"
          ? <div><EditUserForm id={data.location.state.id} email={data.location.state.email} role={data.location.state.role} />
          </div>: <this.AuthError/>
          } />
          
          {
              /**-----------------------------------------------------
               *  Classes
               */
            }

          <Route path="/classes" render={(data ) =><div> 
            {this.state.isAuthenticated && this.state.role ==="ADMIN" ? <div>
              <h2 >Classes</h2>
              <div className="manage-button"><Link to="/addclass">Add class</Link>  </div> 
              <ClassesView />
               </div> : null} 

            {this.state.isAuthenticated && this.state.role ==="TEACHER" ? <this.AuthError/>: null}

            {(this.state.isAuthenticated && this.state.role ==="STUDENT") ? <this.AuthError/> : null}  
            
            
            </div>}/>

            
            
          <Route path="/addclass" render={(data ) =><div> 
            {(this.state.isAuthenticated && this.state.role ==="ADMIN") ?
             <AddClassForm/> :
            <this.AuthError/> }  </div>}  />

            {
              /**-----------------------------------------------------
               *  Tasks
               */
            }
          <Route path="/tasks" render={(data ) =>
          <div>
            {(this.state.isAuthenticated && this.state.role ==="ADMIN") ? <div>
              <h2 className="AssignmentsView-h2">Tasks</h2> </div> : null}

            {(this.state.isAuthenticated && this.state.role ==="TEACHER") ? <div>
              <h2 className="AssignmentsView-h2">Tasks</h2>
              <div className="manage-button"><Link to="/addtask">Add task</Link>  </div> 
            </div> : null}

            {(this.state.isAuthenticated && this.state.role ==="STUDENT") ? <div>
              <h2 className="AssignmentsView-h2">All tasks</h2> </div> : null}

              
              <TasksView/>
             </div>} />
          <Route path="/addtask" render={(data ) => <div> 
            {(this.state.isAuthenticated && this.state.role ==="TEACHER") ? <AddTaskForm /> :<this.AuthError/>}
            </div>} />
          <Route path="/dotask" render={(data ) => <div>
            {(this.state.isAuthenticated && this.state.role ==="STUDENT") ?
             //<DoTask task={data.location.state.task}/> : <this.AuthError/> }
             <DoTask /> : <this.AuthError/> }
             </div>} />


            {
              /**-----------------------------------------------------
               *  Assignments
               */
            }
          <Route path="/assignments" render={(data ) =><div>
            {(this.state.isAuthenticated && this.state.role ==="ADMIN") ? <div>
              <h2 className="AssignmentsView-h2">Assignments</h2> </div> : null}
            {(this.state.isAuthenticated && this.state.role ==="TEACHER") ? <div>
              <h2 className="AssignmentsView-h2">Assignments</h2>
              <div className="manage-button"><Link to="/addassignment">Add assignment</Link>  </div>
            </div> : null}

            {(this.state.isAuthenticated && this.state.role ==="STUDENT") ? <div>
              <h2 className="AssignmentsView-h2">My assignments</h2> </div> : null}

              
             <AssignmentsView/></div>}/>
          <Route path="/addassignment"  render={(data ) => <div>
             {(this.state.isAuthenticated && this.state.role ==="TEACHER") ?<AddAssignmentForm/> :
             <this.AuthError/>}
             </div>} />

        </Switch>
      </Router>
    </div>
    );
    }
  
}
