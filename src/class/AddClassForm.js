import React from 'react';
import './AddClassForm.css';


import {
  Redirect
} from "react-router-dom";


class AddClassForm extends React.Component{
    constructor(props) {
        super(props);
        this.state={
          name:'',
          teacher:'',
          students:[],
          clicked: false,
          isFetching: true,
          users: [
            /*
            this.createUserData(1,"mligeza@student.agh.edu.pl","admin",false),
            this.createUserData(2,"amatys@agh.edu.pl","teacher",false),
            this.createUserData(3,"dnowak@agh.edu.pl","teacher",false),
            this.createUserData(4,"rtokarczyk@student.agh.edu.pl","student",false),
            this.createUserData(5,"sbodo@student.agh.edu.pl","student",false),
            this.createUserData(6,"jkaras@student.agh.edu.pl","student",false),
            this.createUserData(7,"amarny@student.agh.edu.pl","student",false),
            this.createUserData(8,"zwojas@student.agh.edu.pl","student",false),
            this.createUserData(9,"lstuart@student.agh.edu.pl","student",false),
            this.createUserData(10,"random@gmail.com","user",false)
            */
          ]
        }
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeTeacher = this.handleChangeTeacher.bind(this);
        this.getUsers = this.getUsers.bind(this);
        this.handleChangeStudent = this.handleChangeStudent.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      

      async  getUsers() {
        ///*
        try {
          const axios = require('axios').default;
           //response = await  axios.get('http://localhost:8080/classes',{
            var $this = this;
          axios.get('http://localhost:8080/users',{
            headers: {
            'Access-Control-Allow-Origin': '*',
          }}).then(function (response) {
            //console.log(response.data);
            response.data.map((user) => {
              //console.log(user.id+"  "+user.username)
              if($this.state.users === []){
                $this.setState(state => ({
                  users: [$this.createUserData(user.id,user.username)]
               }));
              }else{
                $this.setState (state => ({
                  users: $this.state.users.concat([$this.createUserData(user.id,user.username, user.role)])
               }));
              }
              return $this.state.users;
             })
             $this.setState({
               isFetching:false
             })
             //console.log($this.state.users);
          }).catch(function (error) {
            console.log(error);
            $this.setState({
              isFetching:false
            })
          });
         // */
        
        //console.log(response);
      } catch (error) {
        console.error(error);
      }
    }
      createUserData(id, email, role, visible) {
        return { id, email, role, visible };
      }

      handleChangeName(event) {
        // console.log("Login changed:" +event.target.value);
      this.setState({name: event.target.value});
      }

      handleChangeTeacher(event) {
        // console.log("Login changed:" +event.target.value);
      this.setState({teacher: event.target.value});
      }
      handleChangeStudent(event) {
        // console.log("Login changed:" +event.target.value);
        if(this.state.students.includes(event.target.value)){
          var a=this.state.students.filter(e => e !== event.target.value);
          this.setState({students: a});
        }
        else{
          this.state.students.push(event.target.value)
        }
       //this.setState({students: });
      }

      componentDidMount() {
        this.getUsers()
      }

      handleSubmit(event) {
        //TODO: POST class
        this.setState({clicked: true});
        alert('501 NOT IMPLEMENTED');
        event.preventDefault();
        
      }
      render(){
        return(
            <div className="AddClassForm"> 
              <h2 className="AddClassForm-h2">Add class</h2>
              {this.state.isFetching ? <div>
                Loading...  
              </div> : null}
              {this.state.clicked ? <Redirect
                          to={{
                            pathname: "/classes",
                            
                          }}
                        /> : null}
            <form className="AddClassForm-form" onSubmit={this.handleSubmit}>
              <label className="AddClassForm-label" for="name">Class name: </label>
              <input className="AddClassForm-input"  type="text" value={this.state.name} id="name" name="name" onChange={this.handleChangeName}/> <br/>
              {/*
              <label className="AddClassForm-label" for="name">Due date: </label>
              <input className="AddClassForm-input"  type="text" value={this.state.name} id="name" name="name" onChange={this.handleChangeName}/> <br/>
              */}
              <label className="AddClassForm-label" for="teacher">Teacher: </label>
                <select className="AddClassForm-select" value={this.state.teacher} id="teacher" name="teacher" onChange={this.handleChangeTeacher}>
                  {this.state.users.map((u) => (
                    u.role === 'TEACHER' ? <option className="AddClassForm-option" value={u.id}>{u.email}</option> : null
                  ))

                  }
                </select><br/>

                <label className="AddClassForm-label" for="students">Students: </label>
                
                <select multiple={true} value={this.state.students} id="students" name="students"  onChange={this.handleChangeStudent}>
                {this.state.users.map((u) => (
                    u.role === 'STUDENT' ? <option className="AddClassForm-option" value={u.id}>{u.email}</option> : null
                  ))

                  }
                </select><br/>
                <button className="AddClassForm-button" type="submit">ADD</button>
            </form>

            </div>
        );
      }
   
}

export default AddClassForm;