import React from 'react';
import './ClassesView.css';


import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'

class ClassesView extends React.Component{
    constructor(props) {
        super(props);
        this.state={isFetching: true,
          classes:[]};
        /*this.state={classes:[
          this.createData(1,"1a","amatys@agh.edu.pl", [ "rtokarczyk@student.agh.edu.pl","amarny@student.agh.edu.pl"]),
          this.createData(2,"1b","amatys@agh.edu.pl", [ "sbodo@student.agh.edu.pl","zwojas@student.agh.edu.p"]),
          this.createData(3,"2a","dnowak@agh.edu.p", [ "jkaras@student.agh.edu.pl","lstuart@student.agh.edu.pl"])
        ]};*/
        this.getClasses = this.getClasses.bind(this);
        //this.mapClasses = this.mapClasses.bind(this);
       //this.getClasses();
      }
/*
      mapClasses(classObj){
        this.setState(state => ({
          classes: this.state.classes.concat([this.createData(classObj.id,classObj.name)])
       }));
      }
*/
      async  getClasses() {
          ///*
          try {
            const axios = require('axios').default;
             //response = await  axios.get('http://localhost:8080/classes',{
              var $this = this;
            axios.get('http://localhost:8080/classes',{
              headers: {
              'Access-Control-Allow-Origin': '*',
            }}).then(function (response) {
              //console.log(response.data);
              response.data.map((classObj) => {
                //.log(classObj.id+"  "+classObj.name)
                if($this.state.classess === []){
                  $this.setState(state => ({
                    classes: [$this.createData(classObj.id,classObj.name)]
                 }));
                }else{
                  $this.setState (state => ({
                    classes: $this.state.classes.concat([$this.createData(classObj.id,classObj.name)])
                 }));
                }
                return $this.state.classes;
               })
               $this.setState({
                 isFetching:false
               })
            }).catch(function (error) {
              console.log(error);
              $this.setState({
                isFetching:false
              })
            });
           // */
          
          //console.log(response);
        } catch (error) {
          console.error(error);
        }
        
       
      }
      /*
    id: id,
    name: name,
    teacher: email,
    students: [
        student: email,
        student: email
    ]
      */
      createData(id, name, teacher, students ) {
        
        return { id, name, teacher, students};
      }

      componentDidMount() {
        this.getClasses()
      }
    
      render(){
        return(
            <div className="ClassesView" > 
              {this.state.isFetching ? <div>
                Loading...  
              </div> : null}
              <TableContainer component={Paper}>
                <Table className="ClassesView-table" aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>ID </TableCell>
                      <TableCell align="left">Task name</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.classes!= null ? this.state.classes.map((row) => (
                      <TableRow key={row.name}>
                        <TableCell component="th" scope="row">
                          {row.id}
                        </TableCell>
                        <TableCell align="left">{row.name}</TableCell>
                      </TableRow>
                    )) : null}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
        );
      }
   
}

export default ClassesView;