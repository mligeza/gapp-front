import React from 'react';
import './AddUserForm.css';

import {
  Redirect
} from "react-router-dom";

class AddUserForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {login: '', password: '', clicked: false};
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
      }
      handleInputChange(event) {
        const target = event.target;
        const value = target.name === "isGoing" ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }
      handleChangeLogin(event) {
        // console.log("Login changed:" +event.target.value);
       this.setState({login: event.target.value});
     }
     handleChangePassword(event) {
      // console.log("Login changed:" +event.target.value);
     this.setState({password: event.target.value});
   }

     
     handleSubmit(event) {
       //TODO: POST register
       /*
            {
          "id": 3,
          "username": "test",
          "password": "$2y$12$KWV9Ung46ypGm6iu30Zc9uZQeWNMyzvMJaQ4Gk.JhnidAp3U9L.Em",
          "role": "USER",
          "enabled": true,
          "accountNonExpired": true,
          "credentialsNonExpired": true,
          "accountNonLocked": true
      }
       */
       alert('501 NOT IMPLEMENTED');
       event.preventDefault();
       this.setState({clicked: true});
     }
      render(){
        return(
            <div className="AddUserForm"> 
             {this.state.clicked ? <Redirect
                          to={{
                            pathname: "/",
                            
                          }}
                        /> : null}
              <form className="AddUserForm-form" onSubmit={this.handleSubmit}>
                <h2 className="AddUserForm-h2">REGISTER FORM</h2>
                <label className="AddUserForm-label" for="login">Username:</label><br/>
                <input className="AddUserForm-input" type="text" value={this.state.login} onChange={this.handleInputChange} id="login" name="login"/> <br/>
                <label className="AddUserForm-label" for="password">Password:</label><br/>
                <input className="AddUserForm-input" type="password" value={this.state.password} onChange={this.handleInputChange} id="password" name="password"/><br/>
                <button className="AddUserForm-button" type="submit">REGISTER</button>
              </form>
            </div>
        );
      }
   
}

export default AddUserForm;