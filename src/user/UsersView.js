import React from 'react';
import './UsersView.css';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import {
  Redirect
} from "react-router-dom";

class UsersView extends React.Component{
  

    constructor(props) {
      super(props);
      
      //TODO: GET users
      
        this.state={ selectedId: '',selectedEmail: '',
        selectedRole:'', app: props.app, clicked: false};
     
      this.state={
        isFetching: true,
        users:[
          /*
        this.createData(1,"mligeza@student.agh.edu.pl","admin"),
        this.createData(2,"amatys@agh.edu.pl","teacher"),
        this.createData(3,"dnowak@agh.edu.pl","teacher"),
        this.createData(4,"rtokarczyk@student.agh.edu.pl","student"),
        this.createData(5,"sbodo@student.agh.edu.pl","student"),
        this.createData(6,"jkaras@student.agh.edu.pl","student"),
        this.createData(7,"amarny@student.agh.edu.pl","student"),
        this.createData(8,"zwojas@student.agh.edu.pl","student"),
        this.createData(9,"lstuart@student.agh.edu.pl","student"),
        this.createData(10,"random@gmail.com","user")
        */
      ]};
        this.handleEditUser = this.handleEditUser.bind(this);
        this.getUsers = this.getUsers.bind(this);
        //this.handleAddUser = this.handleAddUser.bind(this);
        //this.handleSubmit = this.handleSubmit.bind(this);
      }
      
      componentDidMount() {
        this.getUsers()
      }

      async  getUsers() {
        ///*
        try {
          const axios = require('axios').default;
           //response = await  axios.get('http://localhost:8080/classes',{
            var $this = this;
          axios.get('http://localhost:8080/users',{
            headers: {
            'Access-Control-Allow-Origin': '*',
          }}).then(function (response) {
            //console.log(response.data);
            response.data.map((user) => {
              //console.log(user.id+"  "+user.username)
              if($this.state.users === []){
                $this.setState(state => ({
                  users: [$this.createData(user.id,user.username)]
               }));
              }else{
                $this.setState (state => ({
                  users: $this.state.users.concat([$this.createData(user.id,user.username, user.role)])
               }));
              }
              return $this.state.users;
             })
             $this.setState({
               isFetching:false
             })
          }).catch(function (error) {
            console.log(error);
            $this.setState({
              isFetching:false
            })
          });
         // */
        
        //console.log(response);
      } catch (error) {
        console.error(error);
      }
    }
      createData(id, email, role) {
        return { id, email, role };
      }

      handleEditUser = (row) => {
        //this.state.app.handleEditUser(row);
        //this.onEdit(row);
        //this.state.app.handleEditUser(row);
        this.setState({
          selectedId: row.id, 
          selectedEmail: row.email,
          selectedRole: row.role
        });
        this.setState({clicked: true});
        console.log(row.id +" "+row.email+" "+row.role);
        //alert(this.selectedId +" "+this.selectedEmail+" "+this.selectedRole+" "+this.state.app);
        
                    
      };
      
      

      render(){

        return(
            <div className="UsersView" id="UsersView"> 
            {this.state.isFetching ? <div>
                Loading...  
              </div> : null}
              {this.state.clicked ? <Redirect
                          to={{
                            pathname: "/edituser",
                            state: { id: this.state.selectedId, email: this.state.selectedEmail, role: this.state.selectedRole }
                          }}
                        /> : null}
       
              <TableContainer component={Paper}>
                <Table className="UsersView-table" aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>ID </TableCell>
                      <TableCell align="left">Email</TableCell>
                      <TableCell align="left">Role</TableCell>
                      {/*<TableCell align="left">EDIT</TableCell>
                      */}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.users.map((row) => (
                      
                        
                      <TableRow key={row.id}  onClick={()=>{this.handleEditUser(row);}}>
                        
                        

                        <TableCell component="th" scope="row">
                          {row.id}
                        </TableCell>
                        <TableCell align="left">{row.email}</TableCell>
                        <TableCell align="left">{row.role}</TableCell>
                        {/*<TableCell align="center">
                          <div className="btn" >
                            <button>EDIT</button>
                          </div>
                        </TableCell>
                      */}
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
        );
      }
   
}

export default UsersView;