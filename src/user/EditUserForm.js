import React from 'react';
import './EditUserForm.css';

import {
  Redirect
} from "react-router-dom";

export default class EditUserForm extends React.Component{
    constructor(props) {
        super(props);
        console.log(this);
        this.state = { id: props.id, email: props.email, role: props.role, clicked: false};
        //this.handleChangeLogin = this.handleChangeLogin.bind(this);
        //this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeRole = this.handleChangeRole.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleChangeRole(event) {
        // console.log("Login changed:" +event.target.value);
       this.setState({role: event.target.value});
     }
     handleSubmit(event) {
      //TODO: PATCH user
      alert('501 NOT IMPLEMENTED');
      event.preventDefault();
      this.setState({clicked: true});
    }

    handleRenderVisible(){
      
       return (<div className="EditUserForm show" > 
          <h2 className="EditUserForm-h2">EDIT USER FORM</h2>
          {this.state.clicked ? <Redirect
                          to={{
                            pathname: "/users",
                            
                          }}
                        /> : null}
          <div className="modal">
            <div className="modal_content">
              {//<span className="close" onClick={this.handleClick}>
                //&times;
              //</span>
              }
              <form className="EditUserForm-form" onSubmit={this.handleSubmit}>
                <label className="EditUserForm-label" for="login">Username: </label>
                <input className="EditUserForm-input" disabled="disabled" type="text" value={this.state.email} id="login" name="login"/> <br/>
                <label className="EditUserForm-label" for="role">Role: </label>
                <select className="EditUserForm-select" value={this.state.role} id="role" name="role" onChange={this.handleChangeRole}>
                  <option className="EditUserForm-option" value="user">User</option>
                  <option className="EditUserForm-option" value="student">Student</option>
                  <option className="EditUserForm-option" value="teacher">Teacher</option>
                  <option className="EditUserForm-option" value="admin">Admin</option>
                </select><br/>
                <button className="EditUserForm-button" type="submit">SAVE</button>
              </form>
            </div>
          </div>
        </div>);
       
    }

      render(){
          return(
           this.handleRenderVisible()
          
          );
    }
  
}