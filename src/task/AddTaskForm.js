import React from 'react';
import './AddTaskForm.css';



import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'

import {
  Redirect
} from "react-router-dom";

/**
 * Exercise{
    type=reading
    materials = text
    question = what tomas did
    anwser = wpisywanie
}{
    type=questons
    materials = possible answers
    question = how many years old
    anwser = proper answer
}{
    type=quiz
    materials = possible answers
    question = how many years old
    anwser = proper answer (but time)
}{
    type=fill
    materials = text
    question = how many years old
    anwser = proper answer (but to fill)
}
 */
class AddTaskForm extends React.Component{
    constructor(props) {
        super(props);
        this.state={
          type:'',
          name:'',
          materials:[],
          question:'',
          answer:'',
          excercises:[],
          clicked: false,
          
        }
        this.handleChangeAddQuestion = this.handleChangeAddQuestion.bind(this);
         this.handleAddExercise = this.handleAddExercise.bind(this);
         this.handleRemoveExercise = this.handleRemoveExercise.bind(this);
         this.handleChangeAddAnswer = this.handleChangeAddAnswer.bind(this);
          this.handleChangeAddType = this.handleChangeAddType.bind(this);
         this.handleChangeAddMaterials = this.handleChangeAddMaterials.bind(this);
         this.handleSubmit = this.handleSubmit.bind(this);
          this.handleChangeName = this.handleChangeName.bind(this);
      }
      
      handleChangeName(event){
        this.setState({name: event.target.value});
      }
      handleChangeAddQuestion(event){
        this.setState({question: event.target.value});
      }
      handleChangeAddType(event){
        this.setState({type: event.target.value});
      }
      
      
      handleAddExercise(event){
        
          this.setState(state => ({
            excercises: this.state.excercises.concat([this.createTaskData(this.state.type,this.state.materials, this.state.question,this.state.answer)]),
            materials:'',
            question:'',
            answer:'',
            clicked: false
          }));
          alert('501 NOT IMPLEMENTED');
          //excercises: [excercises,  this.state.type, materials,question,answer }]
      }
      
      handleRemoveExercise(event){
          this.setState({excercises: this.state.excercises.filter(function(e) {
           //alert(e.question);
           //alert(event.target.value);
            return e.question !== event.target.value 
        })});
       
       // alert('501 NOT IMPLEMENTED');
      }

      handleChangeAddAnswer(event){
        this.setState({answer: event.target.value});
      }
      handleChangeAddMaterials(event){
        this.setState({materials: event.target.value});
      }
      
      createTaskData(type, materials,question,answer ) {
        return { type, materials,question,answer };
      }

      handleSubmit(event) {
        //TODO: POST class
        /*
          Task:
          {
              "id": 3,
              "name": "test task",
              "gappExercise": []
          }

        */
       /*
       axios.post('/user', {
          firstName: 'Fred',
          lastName: 'Flintstone'
        })
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
       */
        alert('501 NOT IMPLEMENTED');
        event.preventDefault();
        this.setState({clicked: true});
      }

      render(){
        return(
            <div className="AddTaskForm"> 
              <h2 className="AddTaskForm-h2">Add Task </h2>
              <form className="AddClassForm-form" onSubmit={this.handleSubmit}>
              <label className="AddClassForm-label" for="name">Task name: </label>
              <input className="AddClassForm-input"  type="text" value={this.state.name} 
              id="name" name="name" onChange={this.handleChangeName}/> <br/><br/>
              {this.state.clicked ? <Redirect
                          to={{
                            pathname: "/tasks",
                            
                          }}
                        /> : null}
              <TableContainer component={Paper}>
                <Table className="ClassesView-table" aria-label="simple table">
                  <TableHead>
                    <TableRow>
           
                      <TableCell align="left">Type</TableCell>
                      <TableCell align="left">Materials</TableCell>
                      <TableCell align="left">Question</TableCell>
                      <TableCell align="left">Answer</TableCell>
                      <TableCell align="left">Action</TableCell>
                    </TableRow>
                    
                  </TableHead>
                  <TableBody>
                  <TableRow>
                    <TableCell align="left">
                    <select className="AddClassForm-select" value={this.state.type} 
                    id="type" name="type" onChange={this.handleChangeAddType}>
                      <option className="AddClassForm-option" value="reading">Reading</option>
                      <option className="AddClassForm-option" value="questions">Question</option>
                      <option className="AddClassForm-option" value="quiz">Quiz</option>
                      <option className="AddClassForm-option" value="fill">Fill</option>
                      </select><br/>
                    </TableCell>
                    <TableCell align="left">
                      <input className="AddClassForm-input"  type="text" value={this.state.materials} 
                      id="materials" name="materials" onChange={this.handleChangeAddMaterials}/> <br/>
                    </TableCell>
                    <TableCell align="left">
                      <input className="AddClassForm-input"  type="text" value={this.state.question} 
                      id="question" name="question" onChange={this.handleChangeAddQuestion}/> <br/>
                    </TableCell>
                    <TableCell align="left">
                    <input className="AddClassForm-input"  type="text" value={this.state.answer} 
                    id="answer" name="answer" onChange={this.handleChangeAddAnswer}/> <br/>
                    </TableCell>
                    <TableCell align="left"><button type="button" onClick={this.handleAddExercise}>Add</button></TableCell>
                  </TableRow>

                    {this.state.excercises.map((e) => (
                      <TableRow key={e.question}>
                        <TableCell component="th" scope="row">
                          {e.type}
                        </TableCell>
                        <TableCell align="left">{e.materials}</TableCell>
                        <TableCell align="left">{e.question}</TableCell>
                        <TableCell align="left">{e.answer}</TableCell>
                        <TableCell align="left"><button type="button" value={e.question} onClick={this.handleRemoveExercise}>Remove</button></TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>

              
                <button className="AddClassForm-button" type="submit">SAVE</button>
            </form>

            </div>
        );
      }
   
}

export default AddTaskForm;