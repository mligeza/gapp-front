import React from 'react';
import './DoTask.css';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
//import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'

export default class DoTask extends React.Component{
    constructor(props) {
        super(props);
        // /*
        this.state={
            assignment: true,
                id: 1,
                gappTask: {
                    id: 1,
                    name: "first task",
                    gappExercise: [
                        {
                            id: 1,
                            type: "question",
                            materials: "materials123",
                            question: "what is the answer?",
                            answer: "my answer"
                        },
                        {
                            id: 2,
                            type: "question",
                            materials: "wisla,bug,odra,ropa",
                            question: "ktora polska rzeka jest najdluzsza?",
                            answer: "wisla"
                        }
                    ]
                },
                dueDate: "2020-12-31T23:00:00.000+00:00",
                gappClass: {},
                gappMarsk: []
            }
            // */
            // /*
            this.state={
            assignment: false,
            gappTask:{
                id: 1,
                name: "first task",
                gappExercise: [
                    {
                        id: 1,
                        type: "question",
                        materials: "materials123",
                        question: "what is the answer?",
                        answer: "my answer"
                    },
                    {
                        id: 2,
                        type: "quiz",
                        materials: "wisla,bug,odra,ropa",
                        question: "ktora polska rzeka jest najdluzsza?",
                        answer: "wisla"
                    }
                ]
            }
        }
      }



      render(){
          return (<div>
            Here you can do the tasks and challenges.
            <form onSubmit={this}>
            {this.state.gappTask.gappExercise.map((exe) => {
            return <div>
                {exe.type==="question" ? <div>
                        {exe.materials}<br/><br/>
                        {exe.question}
                        <input id={"an-"+exe.id} type="text"></input>
                        
                </div>:null}

                
                {exe.type==="fill" ? <div>
                    {exe.materials}<br/><br/>
                    {exe.question}
                    <textarea></textarea>

                </div>:null}



                {exe.type==="quiz" ? <div>
                    {exe.materials}<br/><br/>
                    {exe.question}
                    <select>

                    </select>
                </div>:null}



                {exe.type==="reading" ? <div>
                <TableContainer component={Paper}>
                <Table className="ClassesView-table" aria-label="simple table">
                    <TableBody>
                    <TableRow>
                        <TableCell>
                            {exe.materials}
                        </TableCell>
                    </TableRow>
                    </TableBody>
                    </Table>
                    </TableContainer>
                       
                    {exe.question}
                    <input id={"an-"+exe.id} type="text" onChange=""></input>
                   
                </div>:null}
               
            </div>
        })}
        </form>
          </div>);
      }
    }


    
/*


*/