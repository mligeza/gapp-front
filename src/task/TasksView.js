import React from 'react';
import './TasksView.css';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class TasksView extends React.Component{
    constructor(props) {
        super(props);
        this.state={isFetching: true,
          tasks:[
            /*
          this.createData(1,"Rivers of Poland"),
          this.createData(2,"Polish National Parks"),
          this.createData(3,"Mountains of Poland"),
          this.createData(4,"Cities of Poland"),
          this.createData(5,"Lakes of Poland"),
          this.createData(6,"World islands"),
          this.createData(7,"Geopolitics of Europe"),
          */
        ]};
        this.getTasks = this.getTasks.bind(this);
      }

      async  getTasks() {
        ///*
        try {
          const axios = require('axios').default;
           //response = await  axios.get('http://localhost:8080/classes',{
            var $this = this;
          axios.get('http://localhost:8080/tasks',{
            headers: {
            'Access-Control-Allow-Origin': '*',
          }}).then(function (response) {
            //console.log(response.data);
            response.data.map((task) => {
             // console.log(task.id+"  "+task.name)
              if($this.state.tasks === []){
                $this.setState(state => ({
                  tasks: [$this.createData(task.id,task.name)]
               }));
              }else{
                $this.setState (state => ({
                  tasks: $this.state.tasks.concat([$this.createData(task.id,task.name)])
               }));
              }
              return $this.state.tasks;
             })
             $this.setState({
               isFetching:false
             })
          }).catch(function (error) {
            console.log(error);
            $this.setState({
              isFetching:false
            })
          });
         // */
        
        //console.log(response);
      } catch (error) {
        console.error(error);
      }
      
     
    }

      createData(id, name ) {
        return { id, name };
      }

      componentDidMount() {
        this.getTasks()
      }
      render(){
        return(
            <div className="TasksView"> 
              
              {this.state.isFetching ? <div>
                Loading...  
              </div> : null}
              
              <TableContainer component={Paper}>
                <Table className="UsersView-table" aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>ID </TableCell>
                      <TableCell align="left">Task name</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.tasks.map((row) => (
                      <TableRow key={row.name}>
                        <TableCell component="th" scope="row">
                          {row.id}
                        </TableCell>
                        <TableCell align="left">{row.name}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
        );
      }
   
}

export default TasksView;