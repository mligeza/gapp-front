import React from 'react';
import './AssignmentsView.css';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class AssignmentsView extends React.Component{
    constructor(props) {
        super(props);
        this.state={isFetching: true,
          assignments:[
            /*
          this.createData(1,2,"Polish National Parks",3,"2a","2020/06/20 23:59:59"),
          this.createData(2,3,"Mountains of Poland",2,"1b","2020/06/07 23:59:59"),
          this.createData(3,5,"Lakes of Poland",3,"2a","2020/06/10 17:00:00"),
          this.createData(4,7,"Geopolitics of Europe",3,"2a","2020/06/21 12:00:00"),
          */
        ]};
        this.getAssignments = this.getAssignments.bind(this);
      }
      createData(id, taskID, taskName, classID, className, dueDate ) {
        return { id, taskID, taskName, classID, className, dueDate };
      }
      async  getAssignments() {
        ///*
        try {
          const axios = require('axios').default;
           //response = await  axios.get('http://localhost:8080/classes',{
            var $this = this;
          axios.get('http://localhost:8080/assignments',{
            headers: {
            'Access-Control-Allow-Origin': '*',
          }}).then(function (response) {
            //console.log(response.data);
            response.data.map((assg) => {
              //console.log(assg.id+" "+assg.gappTask.id+" "+assg.gappTask.name+" "+assg.dueDate);
              if($this.state.assignments === []){
                $this.setState(state => ({
                  assignments: [$this.createData(assg.id,assg.gappTask.id,assg.gappTask.name,'','',assg.dueDate)]
               }));
              }else{
                $this.setState (state => ({
                  assignments: $this.state.assignments.concat([$this.createData(assg.id,assg.gappTask.id,assg.gappTask.name,'','',assg.dueDate)])
               }));
              }
              return $this.state.assignments;
             })
             $this.setState({
               isFetching:false
             })
          }).catch(function (error) {
            console.log(error);
            $this.setState({
              isFetching:false
            })
          });
         // */
        //console.log(this.state.assignments)
        //console.log(response);
      } catch (error) {
        console.error(error);
      }
      
     
    }

    componentDidMount() {
      this.getAssignments()
    }
      render(){
        return(
            <div className="AssignmentsView"> 
               {this.state.isFetching ? <div>
                Loading...  
              </div> : null}
              
              <TableContainer component={Paper}>
                <Table className="AssignmentsView-table" aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>ID </TableCell>
                      <TableCell align="left">Task ID</TableCell>
                      <TableCell align="left">Task name</TableCell>
                      <TableCell align="left">Class ID</TableCell>
                      <TableCell align="left">Class name</TableCell>
                      <TableCell align="left">Due date</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.assignments.map((row) => (
                      <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                          {row.id}
                        </TableCell>
                        <TableCell align="left">{row.taskID}</TableCell>
                        <TableCell align="left">{row.taskName}</TableCell>
                        <TableCell align="left">{row.classID}</TableCell>
                        <TableCell align="left">{row.className}</TableCell>
                        <TableCell align="left">{row.dueDate}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
        );
      }
   
}

export default AssignmentsView;