import React from 'react';
import './AddAssignmentForm.css';

import {
  Redirect
} from "react-router-dom";


class AddAssignmentForm extends React.Component{
    constructor(props) {
        super(props);
        this.state={
          class:'',
          task:'',
          clicked: false,
          dueDate: '',
          markType: '',
          isFetchingTasks: true,
          isFetchingClasses: true,
          classes: [
            /*
            this.createClassData(1,"1a","amatys@agh.edu.pl", [ "rtokarczyk@student.agh.edu.pl","amarny@student.agh.edu.pl"]),
            this.createClassData(2,"1b","amatys@agh.edu.pl", [ "sbodo@student.agh.edu.pl","zwojas@student.agh.edu.p"]),
            this.createClassData(3,"2a","dnowak@agh.edu.p", [ "jkaras@student.agh.edu.pl","lstuart@student.agh.edu.pl"])
            */
          ],
          tasks:[
            /*
            this.createTaskData(1,"Rivers of Poland"),
            this.createTaskData(2,"Polish National Parks"),
            this.createTaskData(3,"Mountains of Poland"),
            this.createTaskData(4,"Cities of Poland"),
            this.createTaskData(5,"Lakes of Poland"),
            this.createTaskData(6,"World islands"),
            this.createTaskData(7,"Geopolitics of Europe"),
            */
          ]
        }

        this.handleChangeClass = this.handleChangeClass.bind(this);
        this.handleChangeTask = this.handleChangeTask.bind(this);
        this.handleChangeDueDate = this.handleChangeDueDate.bind(this);
        this.handleChangeMarkType = this.handleChangeMarkType.bind(this);
        this.getTasks= this.getTasks.bind(this);
        this.getClasses = this.getClasses.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      async  getTasks() {
        ///*
        try {
          const axios = require('axios').default;
           //response = await  axios.get('http://localhost:8080/classes',{
            var $this = this;
          axios.get('http://localhost:8080/tasks',{
            headers: {
            'Access-Control-Allow-Origin': '*',
          }}).then(function (response) {
            //console.log(response.data);
            response.data.map((task) => {
             // console.log(task.id+"  "+task.name)
              if($this.state.tasks === []){
                $this.setState(state => ({
                  tasks: [$this.createTaskData(task.id,task.name)]
               }));
              }else{
                $this.setState (state => ({
                  tasks: $this.state.tasks.concat([$this.createTaskData(task.id,task.name)])
               }));
              }
              return $this.state.tasks;
             })
             $this.setState({
              isFetchingTasks:false
             })
          }).catch(function (error) {
            console.log(error);
            $this.setState({
              isFetchingTasks:false
            })
          });
         // */
        
        //console.log(response);
      } catch (error) {
        console.error(error);
      }
      
     
    }
      createTaskData(id, name ) {
        return { id, name };
      }

      async  getClasses() {
        ///*
        try {
          const axios = require('axios').default;
           //response = await  axios.get('http://localhost:8080/classes',{
            var $this = this;
          axios.get('http://localhost:8080/classes',{
            headers: {
            'Access-Control-Allow-Origin': '*',
          }}).then(function (response) {
            //console.log(response.data);
            response.data.map((classObj) => {
              //.log(classObj.id+"  "+classObj.name)
              if($this.state.classess === []){
                $this.setState(state => ({
                  classes: [$this.createClassData(classObj.id,classObj.name)]
               }));
              }else{
                $this.setState (state => ({
                  classes: $this.state.classes.concat([$this.createClassData(classObj.id,classObj.name)])
               }));
              }
              return $this.state.classes;
             })
             $this.setState({
               isFetchingClasses:false
             })
          }).catch(function (error) {
            console.log(error);
            $this.setState({
              isFetchingClasses:false
            })
          });
         // */
        
        //console.log(response);
      } catch (error) {
        console.error(error);
      }
      
     
    }
      createClassData(id, name, teacher, students ) {
        
        return { id, name, teacher, students};
      }
      componentDidMount() {
        this.getClasses();
        this.getTasks();
      }
      
      handleChangeClass(event) {
        // console.log("Login changed:" +event.target.value);
       this.setState({class: event.target.value});
     }
      handleChangeTask(event) {
        // console.log("Login changed:" +event.target.value);
        this.setState({task: event.target.value});
      }
      handleChangeDueDate(event) {
        // console.log("Login changed:" +event.target.value);
        this.setState({dueDate: event.target.value});
      }
      handleChangeMarkType(event) {
        // console.log("Login changed:" +event.target.value);
        this.setState({markType: event.target.value});
      }

      handleSubmit(event) {
        //TODO: POST assignment
        /*
        axios.post('/user', {
    firstName: 'Fred',
    lastName: 'Flintstone'
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
        */
       
        alert('501 NOT IMPLEMENTED');
        event.preventDefault();
        this.setState({clicked: true});
      }

      render(){
        return(
            <div className="AddAssignmentForm"> 
              <h2 className="AddAssignmentForm-h2">Add assignment</h2>
              {this.state.isFetchingTasks && this.state.isFetchingClasses ? <div>
                Loading...  
              </div> : null}
              {this.state.clicked ? <Redirect
                          to={{
                            pathname: "/assignments",
                            
                          }}
                        /> : null}
              <form className="AddAssignmentForm-form" onSubmit={this.handleSubmit}>


                  <label className="AddAssignmentForm-label" for="class">Class: </label>
                <select className="AddAssignmentForm-select" value={this.state.class} id="class" name="class" onChange={this.handleChangeClass}>
                  {this.state.classes.map((c) => (
                    <option className="AddAssignmentForm-option" value={c.id}>{c.name}</option>
                  ))

                  }
                </select><br/>

                <label className="AddAssignmentForm-label" for="task">Task: </label>
                <select className="AddAssignmentForm-select" value={this.state.task} id="task" name="task" onChange={this.handleChangeTask}>
                  {this.state.tasks.map((c) => (
                    <option className="AddAssignmentForm-option" value={c.id}>{c.name}</option>
                  ))

                  }
                </select><br/>                
                
                <label className="AddAssignmentForm-label" for="dueDate">Due date: </label>
                <input className="AddAssignmentForm-input"  type="date" value={this.state.dueDate} id="dueDate" name="dueDate" onChange={this.handleChangeDueDate}/> <br/>

                <label className="AddAssignmentForm-label" for="markType">Mark type: </label>
                <select className="AddAssignmentForm-select" value={this.state.markType} id="markType" name="markType" onChange={this.handleChangeMarkType}>
                    <option className="AddAssignmentForm-option" value="grade">Grades</option>
                    <option className="AddAssignmentForm-option" value="fail">Pass/fail</option>
                </select><br/>    

                <button className="AddAssignmentForm-button" type="submit">ADD</button>
              </form>
            </div>
        );
      }
   
}

export default AddAssignmentForm;